#!/usr/bin/python3


class BrainFuck:
    # If there is an extra ']'
    class ClosingBracketUnmatchedError(Exception):
        pass

    # If the code ended in a loop
    class OpeningBracketUnmatchedError(Exception):
        pass

    # If the input sent to ',' is not equal to 1 byte
    class InputLengthInvalidError(Exception):
        pass

    def __init__(self, code=""):
        """
        self.code: the main BF code itself.
        self.memory: the memory array used to store values
        self.code_index: the current code character that's running
        self.memory_index: the pointer to the memory array
        self.loop_indexes: saves all the indexes of the opening brackets.
        """
        self.code = code
        self.memory = [0]
        self.code_index = self.memory_index = 0
        self.loop_indexes = []

    # sign +
    def sign_plus(self):
        self.memory[self.memory_index] += 1
        self.memory[self.memory_index] %= 256

    # sign -
    def sign_minus(self):
        self.memory[self.memory_index] -= 1
        self.memory[self.memory_index] %= 256

    # sign >
    def sign_right(self):
        self.memory_index += 1

        # If we run out of memory, add another spot
        if self.memory_index >= len(self.memory):
            self.memory.append(0)

    # sign <
    def sign_left(self):
        self.memory_index -= 1

        # Memory wrapping - if we reach the start, loop back to the end
        if self.memory_index < 0:
            self.memory_index = len(self.memory) - 1

    # sign [
    def sign_square_open(self):
        # If the current code should run
        if self.memory[self.memory_index] == 0:
            self.code_index -= 1

        else:
            self.loop_indexes.append(self.code_index)

    # sign ]
    def sign_square_close(self):
        # If we finished running the current loop
        if self.memory[self.memory_index] == 0:
            self.loop_indexes.pop()

        else:
            if len(self.loop_indexes) == 0:
                raise BrainFuck.ClosingBracketUnmatchedError
            # Jump back to the start of the loop
            self.code_index = self.loop_indexes[-1]

    # sign .
    def sign_period(self):
        print(chr(self.memory[self.memory_index]), end='')

    # sign ,
    def sign_comma(self):
        char = input()

        # Length if the character must be 1 byte
        if len(char) != 1:
            raise BrainFuck.InputLengthInvalidError

        self.memory[self.memory_index] = ord(char)

    def print_memory(self):
        print(self.memory)

    functions = {
        '+': sign_plus,
        '-': sign_minus,
        '>': sign_right,
        '<': sign_left,
        '[': sign_square_open,
        ']': sign_square_close,
        '.': sign_period,
        ',': sign_comma,
    }

    def parse(self):
        self.__init__(self.code)

        while self.code_index < len(self.code):
            # Get the current sign
            char_code = self.code[self.code_index]

            # If it is a valid BrainFuck instruction
            if char_code in BrainFuck.functions:
                # Run the function associated with the sign
                char_function = BrainFuck.functions[char_code]
                char_function(self)

            self.code_index += 1

        # If the code ended in a loop
        if len(self.loop_indexes) > 0:
            raise BrainFuck.OpeningBracketUnmatchedError
